import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import classification_report
import snowballstemmer

# stemming 
stemmer = snowballstemmer.stemmer("english")


class TfIdfVect(TfidfVectorizer):
    def build_analyzer(self):
        analyzer = super(TfidfVectorizer, self).build_analyzer()
        return lambda doc: stemmer.stemWords(analyzer(doc))


#  reading CSV file
# the file contains 2654 records

# of which 747 messages are spam (28%) and 1907 are ham (72%)
f = pd.read_csv("spamHam.csv", sep=",")

#
# exchange ham with 1 and spam with 0
f.loc[f["type"] == "ham", "type",] = 1
f.loc[f["type"] == "spam", "type",] = 0

# randomly
f = f.sample(frac=1)

# we take f_x as the message, and f_y as the status (ham or spam)
f_x = f["text"]
f_y = f["type"]

#
# tfidf initialization
# lowercase = True

# ngram_range = tuple
# max_features 
tfIdf = TfIdfVect(encoding="utf-8", stop_words="english", analyzer="word",
                  smooth_idf=False, ngram_range=(1, 2))

#
# data separation in 80% for training and 20% for test
x_train, x_test, y_train, y_test = train_test_split(f_x, f_y, stratify=f_y, test_size=0.2)

# fit the text with the tfidf score
x_trainTfIdf = tfIdf.fit_transform(x_train)

# alpha pentru Additive (Laplace/Lidstone) smoothing
# fit_prior - learn class prior probabilities or not.
# naive bayes multinomial intializare
nbM = MultinomialNB(alpha=1.0)

# transform f_y, which contains 1 and 0 as string, in int
y_train = y_train.astype("int")

# Naive - Bayes
nbM.fit(x_trainTfIdf, y_train)

#
# transform the status of the text into an array
a = np.array(y_test)

#
# prediction
x_testTfIdf = tfIdf.transform(x_test)
pred = nbM.predict(x_testTfIdf)

count = 0

for i in range(len(pred)):
    if a[i] == pred[i]:
        count = count + 1

countSpam = 0

for i in range(len(f_y)):
    if f_y[i] == 0:
        countSpam = countSpam + 1

print()
print("File: spamHam.csv")
print(" The file contains", len(f_y), " records")
print(" Including %d (%.2f %%) are messages spam, and %d (%.2f %%) are ham"
      % (countSpam, countSpam / len(f_y) * 100, len(f_y) - countSpam, (len(f_y) - countSpam) / len(f_y) * 100))
print(" Exact ", count, " from", len(a), " (of the test offered)")
print("Model accuracy: %.2f %%" % (np.mean(pred == a) * 100))

# ------------------------
#
# prediction on the file spamHam2.csv

df = pd.read_csv("spamHam2.csv", sep=",")

df.loc[df["type"] == "ham", "type",] = 1
df.loc[df["type"] == "spam", "type",] = 0

df = df.sample(frac=1)

df_tfidf = tfIdf.transform(df["text"])

pred2 = nbM.predict(df_tfidf)

b = np.array(df["type"])

count = 0

for i in range(len(pred2)):
    if pred2[i] == b[i]:
        count = count + 1

countSpam = 0

for i in range(len(df["type"])):
    if b[i] == 0:
        countSpam = countSpam + 1

print("-----------------------------------")
print("File: spamHam2.csv")
print(" The file contains", len(b), " records")
print(" Including %d (%.2f %%) are messages spam, and %d (%.2f %%) are ham"
      % (countSpam, countSpam / len(b) * 100, len(b) - countSpam, (len(b) - countSpam) / len(b) * 100))
print(" Exact ", count, " from", len(b))
print(" Model accuracy: %.2f %%" % (np.mean(pred2 == b) * 100))

print()
print(" Totally used ", len(f_y) + len(b), " records.")

# metrics - classification report

print()
print("File classification report spamHam.csv:")
a1 = []
pred1 = []
for i in range(len(a)):
    a1.append(a[i])
    pred1.append(pred[i])

print(classification_report(a1, pred1))
